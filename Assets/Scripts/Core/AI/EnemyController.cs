﻿using System;
using UnityEngine;

namespace ROR.Core.AI
{
    public class EnemyController : MonoBehaviour
    {
        private Ship m_ship; 
        
        void Start()
        {
            m_ship = GetComponent<Ship>();
        }

        private bool up = false;
        
        void Update()
        {
            var sh = PlayerInputHandler.PlayerShip;
            m_ship.Target(sh.gameObject.transform.position, sh.gameObject, true);
            m_ship.Move(up ? Vector2.up : Vector2.down);

            var y = m_ship.gameObject.transform.position.y;
            if (y > 3.5f)
            {
                up = false;
            } 
            else if (y < -3.5f)
            {
                up = true;
            }
        }
    }
}