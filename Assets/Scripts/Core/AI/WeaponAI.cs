﻿using Assets.Scripts.MyUnity;
using UnityEngine;

namespace ROR.Core.AI
{
    public interface WeaponAI
    {
        public void Init(Weapon Weapon);
        public void Update();
        public void UpdateTarget(Vector3 position);
        public void UpdateTarget(GameObject m_target);
        public void UpdateShooting(bool shooting);
    }
}