﻿using ROR.Lib;
using UnityEngine;

namespace ROR.Core
{
    public class EffectDefinition
    {
            
    }

    public class HealingEffectEntity : EffectEntity
    {
        
    }
    
    public abstract class EffectEntity
    {
        public LivingEntity Caster { get; private set; }
        public LivingEntity Target { get; private set; }

        private IntervalTimer Timer = new IntervalTimer();
        private EffectDefinition definition;
        protected float MaxTime { get; }
        public bool IsAttached { get; private set; }

        public virtual void Init(LivingEntity Caster, LivingEntity Target, EffectDefinition definition)
        {
            this.Caster = Caster; 
            this.Target = Target;
            Timer.Reset();
        }

        public virtual void OnAdded()
        {
            IsAttached = true;
        }

        public virtual void OnRemoved()
        {
            IsAttached = false;
        }

        public virtual void Clear()
        {
            Caster = null;
            Target = null;
            Timer.Clear();
        }
        public virtual void OnTick() { }
        public virtual bool Join(EffectEntity other)
        {
            return false;
        }

        public void Detach()
        {
            Caster.EffectBar.RemoveEffect(this);
        }

        public void Update(float delta)
        {
            if (!IsAttached) return;
            
            if (Timer.Tick(delta))
            {
                OnTick();
            }

            if (Timer.Time >= MaxTime)
            {
                Detach();
            }
        }

        
    }
}
