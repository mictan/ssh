using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ROR.Core
{
    public class HelthContainer : MonoBehaviour{

        public SimpleBar helthBar;
        public LivingEntity target;
        void Start(){
            helthBar.setColor(Color.red);
        }

        void Update(){
            //Debug.Log(target.HP_NOW);
            if(helthBar.maxValue != target.HP_MAX){
                helthBar.maxValue = target.HP_MAX;
            }
            helthBar.setValue(target.HP_NOW);
        }
    }
}
