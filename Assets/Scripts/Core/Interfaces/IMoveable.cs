﻿using UnityEngine;

namespace ROR.Core.Serialization
{
    public interface IMoveable
    {
        Vector2 Movespeed { get; }
    }
}