﻿using System.Collections.Generic;

namespace ROR.Core.Serialization
{
    public class ProjectileDefinition
    {
        public float Speed = 3f;
        public float Damage = 10f;
        public float MaxDistance = float.MaxValue;
        public float MaxLifetime = float.MaxValue;
        public string Prefab;
        
        public List<SubProjectiles> SubProjectiles;
    }
}