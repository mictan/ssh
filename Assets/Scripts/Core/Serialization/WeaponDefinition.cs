﻿using System;
using System.Collections.Generic;

namespace ROR.Core.Serialization
{
    public class WeaponDefinition : Definition
    {
        public float RotationSpeed = 360f;
        public float MinAngle = float.MaxValue;
        public float MaxAngle = float.MaxValue;
        
        
        public string UsedAmmoId;
        public float UsedAmmoPerShot;

        public ProjectileDefinition Projectile = new ProjectileDefinition();
        
        public Stats Stats;
        public ProjectileSpawn[] ProjectileSpawns;
    }

    public class SubProjectiles
    {
        private int Amount;
        private float Speed;
        private float AngleOffset = 0;
        private float Angle;
        private float Damage;
        private String Prefab;
    }
}