﻿using Assets.Scripts.MyUnity;
using ROR.Core.Serialization;
using UnityEngine;

namespace ROR.Core
{
    public class Ship : LivingEntity, IMoveable
    {
        public float MaxMoveSpeed = 2f; 
        public float Acceleration = 2f;
        

        private Weapon[] m_weapons;
        
        private Vector2 m_moveSpeed = Vector2.zero;

        public Vector2 Movespeed => m_moveSpeed;

        protected override void Start()
        {
            base.Start();
            m_weapons = GetComponentsInChildren<Weapon>();
        }
        
        public void Move (Vector2 direction)
        {
            if (direction == Vector2.zero)
            {
                direction = -m_moveSpeed;
            }
            
            
            if (direction != Vector2.zero)
            {
                direction.Normalize();
                m_moveSpeed += direction * Acceleration * Time.deltaTime;
                if (m_moveSpeed.magnitude > MaxMoveSpeed)
                {
                    m_moveSpeed.Normalize();
                    m_moveSpeed *= MaxMoveSpeed;
                }
            }

            gameObject.transform.position += new Vector3(m_moveSpeed.x * Time.deltaTime, m_moveSpeed.y * Time.deltaTime, 0);
        }

        public void Target(Vector2 targetPosition, GameObject gameObject, bool shootFromController)
        {
            foreach (var m in m_weapons)
            {
                m.Target(targetPosition);
                m.Target(gameObject);
                m.WantToShoot(shootFromController);
            }
        }

        
    }
}