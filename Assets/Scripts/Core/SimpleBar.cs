using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ROR.Core
{
    public class SimpleBar : MonoBehaviour{
        public Slider slider;
        public Image fill;

        public float maxValue{
            get => slider.maxValue;
            set {slider.maxValue = value;}
        }

        public void setColor(Color color){
            fill.color = color;
        }

        public void setValue(float newValue){
            slider.value = newValue;
        }
    }
}