using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ROR.Core
{
    public abstract class SkillEntity{
        public float duration = 100;
        public Sprite image;
        public abstract void Activate(SkillBtn callback);
    }

    //Заглушка
    public class ZSkillEntity: SkillEntity{
        public override void Activate(SkillBtn callback){
            callback.StartRecoil();
        }
    }
}
