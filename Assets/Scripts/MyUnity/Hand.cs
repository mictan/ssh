﻿using System;
using ROR;
using ROR.Core;
using ROR.Lib;
using UnityEngine;

namespace Assets.Scripts.MyUnity
{
    public class Hand : MonoBehaviour
    {
        public Camera Camera;
        private Weapon Weapon;
        
        private Vector3 Pointing;
        public Transform ArmRotationPoint;
        
        
        void OnStart()
        {
            //Camera = GetComponentInParent<Camera>();
            //DebugUtility.HandleErrorIfNullGetComponent<Camera, Hand>(Camera, this, gameObject);
            
            Weapon = GetComponentInChildren<Weapon>();
            DebugUtility.HandleErrorIfNullGetComponent<Weapon, Hand>(Weapon, this, gameObject);
        }

        private float angle = 0;

        void Update()
        {
            Weapon = GetComponentInChildren<Weapon>();
            DebugUtility.HandleErrorIfNullGetComponent<Weapon, Hand>(Weapon, this, gameObject);
            
            
            if (Physics.Raycast(Camera.transform.position, Camera.transform.forward, out RaycastHit hit, 1000, -1, QueryTriggerInteraction.Ignore))
            {
                Pointing = hit.transform.position;
            }
            else
            {
                Pointing = Camera.transform.position + Camera.transform.forward;
            }

            //var angle1 = Vector3.Angle(Weapon.MuzzlePoint.position, ArmRotationPoint.position);
            //var angle2 = Vector3.Angle(Pointing, ArmRotationPoint.position);
            //
            //var da = angle2 - angle1;
            //
            //gameObject.transform.RotateAround(ArmRotationPoint.position, -ArmRotationPoint.right, -da);

            Debug.DrawLine(Weapon.MuzzlePoint.position, Pointing, Color.red, 0.3f);
            Debug.DrawLine(Camera.transform.position, Pointing, Color.blue, 0.3f);
        }
    }
}