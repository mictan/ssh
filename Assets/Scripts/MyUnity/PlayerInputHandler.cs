﻿using ROR.Core;
using Unity.FPS.Game;
using UnityEngine;
using UnityEngine.UIElements;

namespace ROR
{
    public class PlayerInputHandler : MonoBehaviour
    {
        public static PlayerInputHandler Instance;
        public static Ship PlayerShip => Instance.m_entity;
        
        private Ship m_entity;
        private Vector2 m_moveIndicator = Vector2.zero;
        private Vector2 m_mousePosition = Vector2.zero;
        private Vector3 m_worldPosition = Vector3.zero;
        
        void Start()
        {
            Instance = this;
            m_entity = GetComponent<Ship>();
            //Cursor.lockState = CursorLockMode.Locked;
            //Cursor.visible = false;
        }

        private const int LMB = 0;
        private const int RMB = 1;
        private const int MMB = 2;

        void Update()
        {
            m_moveIndicator.x = 0;
            m_moveIndicator.y = 0;
            
            m_mousePosition.x = Input.mousePosition.x;
            m_mousePosition.y = Input.mousePosition.y;
            
            if (Input.GetKey(KeyCode.W)) m_moveIndicator.y += 1;
            if (Input.GetKey(KeyCode.A)) m_moveIndicator.x -= 1;
            if (Input.GetKey(KeyCode.S)) m_moveIndicator.y -= 1;
            if (Input.GetKey(KeyCode.D)) m_moveIndicator.x += 1;

            m_entity.Move(m_moveIndicator);

            
            m_worldPosition = Camera.current.ScreenToWorldPoint(m_mousePosition);
            m_entity.Target(m_worldPosition, null, Input.GetMouseButton(LMB));
        }
    }
}