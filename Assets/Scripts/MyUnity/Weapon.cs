﻿using System;
using ROR;
using ROR.Core;
using ROR.Core.AI;
using ROR.Core.Serialization;
using ROR.Lib;
using UnityEngine;

namespace Assets.Scripts.MyUnity
{
    public static class Math3
    {
        public static float FindClosestPointOfApproach(Vector3 currentPosition, Vector3 currentSpeed, Vector3 targetPosition, Vector3 BreakSpeed)
        {
            Vector3 PVec = currentPosition - targetPosition;
            Vector3 SVec = currentSpeed - BreakSpeed;
            float d = SVec.sqrMagnitude;
            // if d is 0 then the distance between Pos1 and Pos2 is never changing
            // so there is no point of closest approach... return 0
            // 0 means the closest approach is now!
            if (d >= -0.0001f && d <= 0.0002f)
                return 0.0f;
            return (-Vector3.Dot(PVec, SVec) / d);
        }
        
        public static Vector3 CalculateInterceptionPoint3D(Vector3 shooterPosition, float bulletSpeed, Vector3 targetPosition, Vector3 targetSpeed, Vector3 shooterSpeed, out float time)
        {
            return CalculateInterceptionPoint3D(shooterPosition, bulletSpeed, targetPosition, targetSpeed - shooterSpeed, out time);
        }

        public static Vector3 CalculateInterceptionPoint3D(Vector3 shooterPosition, float bulletSpeed, Vector3 targetPosition, Vector3 targetSpeed, out float time)
        {
            time = 0f;
            //! Distance between turret and target
            Vector3 D = shooterPosition - targetPosition;
 
            //! Scale of distance vector
            float d = D.magnitude;
 
            //! Speed of target scale of VR
            float SR = targetSpeed.magnitude;
 
            //% Quadratic EQUATION members = (ax)^2 + bx + c = 0
 
            float a = Mathf.Pow(bulletSpeed, 2) - Mathf.Pow(SR, 2);
 
            float b = 2 * Vector3.Dot(D, targetSpeed);
 
            float c = -Vector3.Dot(D, D);
 
            if ((Mathf.Pow(b, 2) - (4 * (a * c))) < 0) //% The QUADRATIC FORMULA will not return a real number because sqrt(-value) is not a real number thus no interception
            {
                return Vector2.zero;//TODO: HERE, PREVENT TURRET FROM FIRING LASERS INSTEAD OF MAKING LASERS FIRE AT ZERO!
            }
            //% Quadratic FORMULA = x = (  -b+sqrt( ((b)^2) * 4*a*c )  ) / 2a
            float t = (-(b) + Mathf.Sqrt(Mathf.Pow(b, 2) - (4 * (a * c)))) / (2 * a);//% x = time to reach interception point which is = t
 
            //% Calculate point of interception as vector from calculating distance between target and interception by t * VelocityVector
            return ((t * targetSpeed) + targetPosition);
        }
    }

    
    public class MouseAIM : WeaponAI
    {
        //Setted from outside
        private Vector3 m_targetPosition;
        private GameObject m_target;
        private bool m_wantShoot;
        private Weapon m_weapon;
        private bool m_alwaysShoot = true; 
        private bool m_aimTargets = true; 
        
        //Internal from outside
        private float m_targetRotation; 
        private Vector3 m_shootDirection;

        public void Init(Weapon weapon)
        {
            this.m_weapon = weapon;
        }
        
        public void UpdateTarget(Vector3 position)
        {
            m_targetPosition = position;
        }
        
        public void UpdateTarget(GameObject target)
        {
            m_target = target;
        }

        public void UpdateShooting(bool shooting)
        {
            m_wantShoot = shooting;
        }

        public void Update()
        {
            bool found = false;
            if (m_target != null)
            {
                found |= InterceptTarget();
            }

            if (!found)
            {
                found |= AIM(m_targetPosition);
            }

            if (!found)
            {
                m_shootDirection = m_weapon.MuzzlePoint.position + m_weapon.MuzzlePoint.right;
            }

            if ((m_wantShoot || m_alwaysShoot) && m_weapon.Enabled && m_weapon.IsBulletLoaded)
            {
                Shoot();
            }
        }


        private bool InterceptTarget()
        {
            
            Vector3 myspeed = new Vector3(m_weapon.m_ship.Movespeed.x, m_weapon.m_ship.Movespeed.y);
            Vector3 speed;
            var movable = m_target.GetComponentInChildren<IMoveable>();
            if (movable != null)
            {
                var ms = movable.Movespeed;
                speed = new Vector3(ms.x, ms.y, 0);
            }
            else
            {
                speed = Vector3.zero;
            }
                
            

            var direction = Math3.CalculateInterceptionPoint3D(
                m_weapon.transform.position,
                m_weapon.ProjectileSpeed,
                m_target.transform.position,
                speed, 
                myspeed,out var time);

            return AIM(m_target.transform.position + direction);
        }
        
        public bool AIM(Vector3 position)
        {
            var def = m_weapon.WeaponDefinition;
            if (def.MaxAngle - def.MinAngle < float.Epsilon && def.MaxAngle < 360f) //Can't rotate
            {
                return false; 
            }

            m_shootDirection = position - m_weapon.transform.position;
            m_targetRotation = 0f; //TODO;

            return true;
        }
        
        private void Shoot()
        {
            m_weapon.Shoot(m_shootDirection);
        }
    }

    
    
    
    

    public interface IActiveComponent
    {
        bool Enabled { get; set; }
    }
    
    public class Weapon : MonoBehaviour, IActiveComponent
    {
        
        public GameObject Bullet;
        public Transform MuzzlePoint;
        public bool Mirrored = false;
        public bool Enabled { get; set; } = true;
        
        private IntervalTimer Timer = new IntervalTimer(0, 0.25f);
        internal Ship m_ship;

        internal float Rotation;
        internal WeaponAI AI;
        internal WeaponDefinition WeaponDefinition = new WeaponDefinition();
        
        internal float ProjectileSpeed => WeaponDefinition.Projectile.Speed;
        
        public bool IsBulletLoaded { get; private set; } = true;

        
        void Start()
        {
            AI = new MouseAIM();
            AI.Init(this);
            
            if (Bullet == null)
            {
                throw new Exception("Bullet not set");
            }
            m_ship = GetComponentInParent<Ship>();
        }

        void Update()
        {
            if (!Enabled) return;
            
            if (!IsBulletLoaded && Timer.Tick(Time.deltaTime))
            {
                IsBulletLoaded = true;
                Timer.Reset();
            }
            
            AI.Update();
        }

        public void WantToShoot(bool canShoot)
        {
            AI.UpdateShooting(canShoot);
        }
        
        public void Target(GameObject m_target)
        {
            AI.UpdateTarget(m_target);
        }
        
        public void Target(Vector3 targetPoint)
        {
            AI.UpdateTarget(targetPoint);
        }

        internal void Shoot(Vector3 direction)
        {
            IsBulletLoaded = false;
            var bullet = Instantiate(Bullet, MuzzlePoint.position, Quaternion.identity);
            var projectile = bullet.GetComponent<Projectile>();
            var velocity = (direction).normalized * ProjectileSpeed;
            projectile.Init(velocity, m_ship);
        }

        
    }
}