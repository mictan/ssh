using UnityEngine;
using UnityEngine.UI;
using System;

namespace ROR.Core
{
    public class SkillBtn : MonoBehaviour{
        public float duration;
        private float current = 0;
        public Image progressbar;        
        public Text timerText;
        public SkillEntity skill = new ZSkillEntity();

        public KeyCode key = KeyCode.None;

        // Update is called once per frame
        void Update(){
            if(current > 0){
                current -= Time.deltaTime;
                if(current <= 0){
                    current = 0;
                    progressbar.enabled = false;
                    timerText.enabled = false;
                } else {
                    progressbar.fillAmount = current / duration;
                    timerText.text = toDurationString(current);
                }
            }

            if(key != KeyCode.None && Input.GetKey(key)){
                StartSkill();
            }
        }

        public bool StartSkill(){
            if(current == 0){
                Debug.Log("start skill!!!");
                current = -1;//duration;
                progressbar.enabled = true;
                progressbar.fillAmount = 1;
                skill.Activate(this);
                return true;
            } else {
                Debug.Log("skill not finished: " + current);
                return false;
            }
        }

        public void StartRecoil(){
            current = duration;
            timerText.enabled = true;
            timerText.text = toDurationString(current);
        }

        private string toDurationString(float seconds){
            if(seconds > 3600){
                return (Convert.ToInt32(seconds / 360) / 10f) + "ч";
            }
            if(seconds > 60){
                return (Convert.ToInt32(seconds / 6) / 10f) + "м";
            }
            return Convert.ToInt32(seconds) + "с";
        }
    }
}
